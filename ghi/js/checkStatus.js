// Get the cookie out of the cookie store
const payloadCookie = await cookieStore.get('jwt_access_payload');

if (payloadCookie) {

  // The cookie value is a JSON-formatted string, so parse it
  const encodedPayload = JSON.parse(payloadCookie.value);

  // Convert the encoded payload from base64 to normal string
  const decodedPayload = atob(encodedPayload);


  // The payload is a JSON-formatted string, so parse it
  const payload = JSON.parse(decodedPayload);


  // Print the payload
  console.log(payload);

  // Check if "events.add_conference" is in the permissions.
  let perms = payload.user.perms;
  let loginTag = document.getElementById('loggedIn');
  let loginTag2 = document.getElementById('loggedIn2')
  let loginTag3 = document.getElementById('loggedin3')

  if (perms.includes('events.add_conference')) {
    loginTag.classList.remove('d-none');
    loginTag2.classList.remove('d-none');
    loginTag3.classList.remove('d-none');

  }
  // If it is, remove 'd-none' from the link


  // Check if "events.add_location" is in the permissions.
  // If it is, remove 'd-none' from the link

  if (perms.includes('events.add_location')) {
    loginTag.classList.remove('d-none');
    loginTag2.classList.remove('d-none');
    loginTag3.classList.remove('d-none');
  }




}



